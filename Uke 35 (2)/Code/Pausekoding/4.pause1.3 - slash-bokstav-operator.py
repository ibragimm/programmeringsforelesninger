# Vi vil skrive ut (printe) en email, men vi vil ikke trenge å skrive ut en linje per print.
# How? Ved å bruke \n. \n er et "tegn" som i en string betyr newline (samme som skjer når du trykker på enter).
# \t finnes, også. Den putter inn en tab. Kjekt!

## Emailen:
# Hei, Anders!
# Har du gjort den tingen?
# Mvh Paul

print('Hei, Anders! \nHar du gjort den tingen? \nMvh Paul')
